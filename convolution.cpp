#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>

using namespace cv;

int main(int argc, char *argv[])
{
	Mat im = imread("im1.jpg");
	Mat out = Mat::zeros(im.size(), im.type());

	//Mat kern = (Mat_<char>(3,3) << 0,-1,0, -1,5,-1, 0,-1,0);
	Mat kern = (Mat_<char>(3,3) << -1,-1,-1, -1,9,-1, -1,-1,-1);
/*	Mat kern = (Mat_<float>(3,3) << 	0.075,0.124,0.075,
						0.124,0.204,0.124,
						0.075,0.124,0.075);*/

	filter2D( im, out, im.depth(), kern );
	
	namedWindow("before", 1);
	namedWindow("after", 1);

	imshow("before", im);
	imshow("after", out);

	waitKey();

	return 0;
}
