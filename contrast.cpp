#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>

using namespace cv;

double alpha;
int beta;

int main()
{
	Mat image = imread("im1.jpg");
	Mat output = Mat::zeros(image.size(), image.type());

	std::cout << "Enter aplha value [1.0 - 3.0]:" << std::endl;
	std::cin >> alpha;
	std::cout << "Enter beta value [0 - 100]:" << std::endl;
	std::cin >> beta;

	// Use this function or the for loops to access pixels
	//image.convertTo(new_image, -1, alpha, beta);
	for( int y = 0; y < image.rows; y++ )
		for( int x = 0; x < image.cols; x++ )
			for( int c = 0; c < 3; c++ )
				output.at<Vec3b>(y,x)[c] = saturate_cast<uchar>( alpha*( image.at<Vec3b>(y,x)[c] ) + beta );

	namedWindow("Original Image", 1);
	namedWindow("New Image", 1);

	imshow("Original Image", image);
	imshow("New Image", output);

	waitKey();

	return 0;
}
