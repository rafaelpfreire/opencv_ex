# README #


### What is this repository for? ###

* Share examples using OpenCV library

### Resources needed to compile the examples ###
* 2 images named "im1.jpg" and "im2.jpg" in the same dir
* Edit Makefile: change "MYAPP" variable to the name of the example you want to compile (ex: MYAPP=contrast)
* Run "make" in the command line to compile the example.