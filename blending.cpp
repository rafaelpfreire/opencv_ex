#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>

using namespace cv;

int main( int argc, char** argv )
{
	double alpha = 0.5;
	double beta;
	double input;

	Mat src1, src2, dst;

	std::cout << "Choose a alpha [0-1]:" << std::endl;
	std::cin >> input;

	if ( input >= 0 && input <= 1.0 )
		alpha = input;

	src1 = imread("im1.jpg");
	src2 = imread("im2.jpg");

	if( !src1.data ) { printf("Error loading src1 \n"); return -1; }
	if( !src2.data ) { printf("Error loading src2 \n"); return -1; }

	beta = 1.0 - alpha;
	namedWindow("Linear Blend", 1);
	addWeighted(src1, alpha, src2, beta, 0.0, dst);

	imshow("Linear Blend", dst);

	waitKey(0);
	return 0;
}
