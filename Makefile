
# --- Name of the App -------------
MYAPP=convolution

# --- C Compiler ------------------
CC=g++

# --- Include Path ----------------
INCLUDE=`pkg-config opencv --cflags`

# --- Libraries -------------------
LIBS=`pkg-config opencv --libs`

# --- Compiler Flags --------------
CFLAGS=-Wall

# --- Install Dir -----------------
INSTDIR=/usr/local/bin


all: $(MYAPP)

$(MYAPP): $(MYAPP).o
	$(CC) $(CFLAGS) -o $(MYAPP) $(MYAPP).o $(INCLUDE) $(LIBS)

main.o: $(MYAPP).cpp

clean:
	-rm main.o

install: $(MYAPP)
	@if [ -d $(INSTDIR) ]; then \
		cp $(MYAPP) $(INSTDIR);\
		chmod a+x $(INSTDIR)/$(MYAPP);\
		chmod og-w $(INSTDIR)/$(MYAPP);\
		echo "Installed in $(INSTDIR)";\
	else \
		echo "Sorry, $(INSTDIR) do not exist";\
	fi


